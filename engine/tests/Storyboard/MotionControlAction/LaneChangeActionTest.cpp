/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/MotionControlAction/LaneChangeAction_impl.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using testing::_;
using testing::Return;

TEST(LaneChangeAction, GivenMatchingControlStrategyGoalIsReached_ReturnTrue)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kPerformLaneChange))
      .WillByDefault(Return(true));

  OpenScenarioEngine::v1_1::LaneChangeAction laneChangeAction({std::vector<std::string>{"Vehicle1"},
                                                               0.0,
                                                               TransitionDynamics{},
                                                               []() -> UniqueId { return 0; }},
                                                              {mockEnvironment});

  ASSERT_TRUE(laneChangeAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(LaneChangeAction, GivenUnmatchingControlStrategyGoalIsReached_ReturnsFalse)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kUndefined))
      .WillByDefault(Return(false));

  OpenScenarioEngine::v1_1::LaneChangeAction laneChangeAction({std::vector<std::string>{"Vehicle1"},
                                                               0.0,
                                                               TransitionDynamics{},
                                                               []() -> UniqueId { return 0; }},
                                                              {mockEnvironment});

  ASSERT_FALSE(laneChangeAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(LaneChangeAction, GivenControlStrategyElements_SetUpControlStrategy)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  std::vector<std::shared_ptr<ControlStrategy>> control_strategies{};

  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(0, testing::_))
      .Times(1)
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::LaneChangeAction laneChangeAction({std::vector<std::string>{"Vehicle1"},
                                                               0.0,
                                                               TransitionDynamics{},
                                                               []() -> UniqueId { return 0; }},
                                                              {mockEnvironment});

  EXPECT_NO_THROW(laneChangeAction.SetControlStrategy());
  auto control_strategy = dynamic_cast<PerformLaneChangeControlStrategy*>(control_strategies[0].get());
  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_NE(nullptr, control_strategy);
  ASSERT_EQ(ControlStrategyType::kPerformLaneChange, control_strategy->type);
  ASSERT_EQ(units::length::meter_t{0.0}, control_strategy->target_lane_offset);
  ASSERT_EQ(UniqueId{0}, control_strategy->target_lane_id);
}