/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../Common/Rule.h"

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>

namespace OPENSCENARIO
{
class SimulationTimeCondition
{
  public:
    SimulationTimeCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISimulationTimeCondition> condition,
                            std::shared_ptr<mantle_api::IEnvironment> environment)
        : environment_{environment}, rule_{condition->GetRule(), condition->GetValue()}
    {
    }

    bool IsSatisfied() const;

  private:
    std::shared_ptr<mantle_api::IEnvironment> environment_;
    OPENSCENARIO::Rule<double> rule_;
};

}  // namespace OPENSCENARIO
