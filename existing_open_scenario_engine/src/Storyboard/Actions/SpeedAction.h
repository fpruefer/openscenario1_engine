/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "MotionControlAction.h"

#include <vector>

namespace OPENSCENARIO
{

class SpeedAction : public MotionControlAction
{
  public:
    SpeedAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISpeedAction> speed_action_data,
                std::shared_ptr<mantle_api::IEnvironment> environment,
                const std::vector<std::string>& actors);

  private:
    void StartAction() override;
    bool HasControlStrategyGoalBeenReached(const std::string& actor) override;
    bool IsLongitudinal() override { return true; }
    bool IsLateral() override { return false; }
    units::velocity::meters_per_second_t GetTargetSpeed(NET_ASAM_OPENSCENARIO::v1_1::ISpeedActionTarget* speed_action_target);
    void SetLinearVelocitySplineControlStrategy(const std::string& actor,
                                                units::velocity::meters_per_second_t target_speed,
                                                NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamics* transition_dynamics);

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISpeedAction> speed_action_data_;
};

}  // namespace OPENSCENARIO
