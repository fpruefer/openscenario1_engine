/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/IState.h"

#include <memory>

namespace OPENSCENARIO
{
class IState;
}

namespace OPENSCENARIO
{

class IStateMachine
{
  public:
    virtual ~IStateMachine() = default;

    virtual void Step() = 0;

    virtual void SetActiveState(IState& new_active_state) = 0;

    virtual const IState* GetActiveState() = 0;
};

}  // namespace OPENSCENARIO
